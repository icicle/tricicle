from simple_pid import PID
from typing import Callable, Tuple
from types import TracebackType
import logging

from icicle.instrument import Instrument
from icicle.scpi_instrument import ValidationError

logger = logging.getLogger(__name__)


def _clamp(value, limits):
    lower, upper = limits
    if value is None:
        return None
    elif (upper is not None) and (value > upper):
        return upper
    elif (lower is not None) and (value < lower):
        return lower
    return value


class PIDController(PID):

    def __init__(
        self,
        measure_channel: Instrument.MeasureChannel,
        set_channel: Instrument.PowerChannel,
        set_field: str = "voltage",
        Kp: float = 1.0,
        Ki: float = 0.0,
        Kd: float = 0.0,
        setpoint: float = 0,
        sample_time: float = 0.01,
        output_limits: Tuple[float, float] = (None, None),
        auto_mode: bool = True,
        proportional_on_measurement: bool = False,
        differential_on_measurement: bool = True,
        error_map: Callable[[float], float] = None,
        time_fn: Callable[[], float] = None,
        starting_output: float = 0.0,
    ) -> None:
        super().__init__(
            Kp=Kp,
            Ki=Ki,
            Kd=Kd,
            setpoint=setpoint,
            sample_time=sample_time,
            output_limits=output_limits,
            auto_mode=auto_mode,
            proportional_on_measurement=proportional_on_measurement,
            differential_on_measurement=differential_on_measurement,
            error_map=error_map,
            time_fn=time_fn,
            starting_output=starting_output,
        )
        self._measure = measure_channel
        self._set = set_channel
        self._set_field = set_field
        self._current_output = starting_output
        self._current_error = 0.0
        self._current_output = starting_output

    def __call__(self) -> None:
        if not self.connected:
            raise RuntimeError(
                "MeasureChannel and PowerChannel not connected - please "
                "make sure to use with... or call __enter__() on "
                "PIDController before using."
            )

        measurement = self._measure.value
        new_control = super().__call__(measurement)
        try:
            setattr(self._set, self._set_field, new_control)
        except ValidationError as ve:
            logger.error(
                f"ValidationError: {str(ve)}\n"
                "-> preventing write of value to device!"
            )
            new_control = self._current_output
            self._last_output = self._current_control
            self._last_error = self._current_error
            self._last_input = self._current_input
        self._current_control = self._last_output
        self._current_error = self._last_error
        self._current_input = self._last_input
        return (measurement, new_control)

    def __enter__(self) -> "PIDController":
        self._measure.__enter__()
        self._set.__enter__()
        return self

    def __exit__(
        self,
        exception_type: type,
        exception_value: BaseException,
        exception_traceback: TracebackType,
    ) -> None:
        self._measure.__exit__(exception_type, exception_value, exception_traceback)
        self._set.__exit__(exception_type, exception_value, exception_traceback)

    def on(self):
        self._set.state = 1

    def off(self):
        self._set.state = 0

    @property
    def measure_channel(self) -> Instrument.MeasureChannel:
        return self._measure

    @measure_channel.setter
    def measure_channel(self, value: Instrument.MeasureChannel):
        if self.auto_mode:
            raise RuntimeError(
                "Cannot replace MeasureChannel whilst PID Controller is active "
                "(i.e. in auto mode)"
            )
        self._measure = value

    @property
    def set_channel(self) -> Instrument.PowerChannel:
        return self._set

    @set_channel.setter
    def set_channel(self, value: Instrument.PowerChannel):
        if self.auto_mode:
            raise RuntimeError(
                "Cannot replace PowerChannel whilst PID Controller is active "
                "(i.e. in auto mode)"
            )
        self._set = value

    @property
    def set_field(self) -> str:
        return self._set_field

    @set_field.setter
    def set_field(self, value: str):
        if self.auto_mode:
            raise RuntimeError(
                "Cannot replace PowerChannel whilst PID Controller is active "
                "(i.e. in auto mode)"
            )
        self._set_field = value

    @property
    def connected(self) -> bool:
        return self._measure.connected and self._set.connected

    def new_starting_control_value(self, value):
        self.reset()
        self._integral = _clamp(self._integral, self.output_limits)
