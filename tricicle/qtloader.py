from importlib import import_module
import pkg_resources
import logging

logger = logging.getLogger(__name__)
dist = None

for package in ("PyQt6", "PyQt5"):
    try:
        dist = pkg_resources.get_distribution(package)
        logger.info(f"{dist.key} ({dist.version}) is installed")
        break
    except pkg_resources.DistributionNotFound:
        logger.info(f"{package} is NOT installed")

if dist is None:
    raise RuntimeError("PyQt not found; accepted versions: PyQt5, PyQt6")

package_map = {
    "pyqt6": "PyQt6",
    "pyqt5": "PyQt5",
}


def load_qt(module, *names):
    mod = import_module(
        f".{module}" if not module.startswith(".") else module,
        package=package_map.get(dist.key),
    )
    if len(names) > 1:
        return tuple(getattr(mod, name) for name in names)
    elif len(names) == 1:
        return getattr(mod, names[0])
    else:
        return mod


def load_ui(module, *names):
    mod = import_module(
        f".{module}" if not module.startswith(".") else module,
        package=f"tricicle.ui_{dist.key[2:].lower()}",
    )
    if len(names) > 1:
        return tuple(getattr(mod, name) for name in names)
    elif len(names) == 1:
        return getattr(mod, names[0])
    else:
        return mod
