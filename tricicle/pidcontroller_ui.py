import logging
import click
from typing import Tuple
from math import inf

from icicle.instrument import Instrument

from .ui.mainwindow import MainWindow

from icicle.hmp4040 import HMP4040  # noqa: F401
from icicle.tti import TTI  # noqa: F401
from icicle.itkdcsinterlock import ITkDCSInterlock  # noqa: F401
from icicle.keysighte3633a import KeysightE3633A  # noqa: F401
from icicle.hp34401a import HP34401A  # noqa: F401
from icicle.keithley2000 import Keithley2000  # noqa: F401
from icicle.keithley2410 import Keithley2410  # noqa: F401
from icicle.relay_board import RelayBoard  # noqa: F401
from icicle.binder_climate_chamber import Binder  # noqa: F401
from icicle.mp1 import MP1  # noqa: F401
from icicle.lauda import Lauda  # noqa: F401
from icicle.cli_utils import verbosity

from .qtloader import load_qt

QApplication = load_qt("QtWidgets", "QApplication")


@click.command(
    "pidcontroller-ui",
    help=("Start PIDController Qt GUI."),
)
@click.option(
    "-v", "--verbose", count=True, help="Verbose output (-v = INFO, -vv = DEBUG)"
)
@click.option(
    "-c",
    "--config",
    type=click.File("r"),
    default=None,
    help=(
        "Config file specifying one or multiple PID Controller setups to be loaded "
        "on initialisation of the GUI."
    ),
)
@click.option(
    "-P",
    "--power-channel",
    type=(
        click.Choice(Instrument.registered_classes),
        str,
        int,
        click.Choice(("voltage", "current")),
    ),
    metavar="POWER_INSTRUMENT RESOURCE CHANNEL voltage|current",
    help="PowerChannel instrument type, resource, channel number, and mode.",
    default=None,
)
@click.option(
    "-M",
    "--measure-channel",
    type=(click.Choice(Instrument.registered_classes), str, int, str),
    metavar="MEASURE_INSTRUMENT RESOURCE CHANNEL MEASURE_TYPE",
    help="MeasureChannel instrument type, resource, channel number, and type.",
    default=None,
)
@click.option(
    "-S",
    "--simulate",
    is_flag=True,
    help="Use pyvisa_sim backend as simulated instruments.",
)
@click.option(
    "-T",
    "--tunings",
    metavar="Kp Ki Kd",
    type=(float, float, float),
    default=(1.0, 0.1, 0.05),
    help="Kp, Ki, Kd tuning parameters for PID loop equation",
)
@click.option(
    "-X",
    "--setpoint",
    metavar="SETPOINT",
    type=float,
    default=0.0,
    help="Initial setpoint for PID loop (default: 0)",
)
@click.option(
    "-I",
    "--starting-output",
    metavar="VALUE",
    type=float,
    default=0.0,
    help="Starting output for channel/PID equation",
)
@click.option(
    "-D",
    "--sample-time",
    metavar="DELAY",
    type=float,
    default="0.01",
    help="Delay between update step of PID equation and power channel (in s)",
)
@click.option(
    "-L",
    "--limits",
    metavar="LOW HIGH",
    type=(float, float),
    default=(-inf, inf),
    help="Limits on output variable",
)
@click.option(
    "--proportional-on-measurement",
    is_flag=True,
    help=(
        "To eliminate overshoot in certain types of systems, calculate the "
        "proportional term directly on the measurement instead of the error."
    ),
)
@click.option(
    "--no-differential-on-measurement",
    is_flag=True,
    help=(
        "By default the differential term is calculated on the measurement rather "
        "than the error; this can be disabled using this flag."
    ),
)
def ui(
    config: click.File,
    power_channel: Tuple[str, int, str, str],
    measure_channel: Tuple[str, int, str, str],
    verbose: int,
    simulate: bool,
    tunings: Tuple[float, float, float],
    setpoint: float,
    starting_output: float,
    sample_time: float,
    limits: Tuple[float, float],
    proportional_on_measurement: bool,
    no_differential_on_measurement: bool,
):
    logging.basicConfig(level=verbosity(verbose))
    configs = list()
    if power_channel is not None and measure_channel is not None:
        configs.append(
            {
                "power_instrument": power_channel[0],
                "power_resource": power_channel[1],
                "power_channel": power_channel[2],
                "power_type": power_channel[3],
                "measure_instrument": measure_channel[0],
                "measure_resource": measure_channel[1],
                "measure_channel": measure_channel[2],
                "measure_type": measure_channel[3],
                "simulate": simulate,
                "Kp": tunings[0],
                "Ki": tunings[1],
                "Kd": tunings[2],
                "setpoint": setpoint,
                "starting_output": starting_output,
                "sample_time": sample_time,
                "output_limits": limits,
                "proportional_on_measurement": proportional_on_measurement,
                "differential_on_measurement": not no_differential_on_measurement,
            }
        )
    if config:
        clist = list()
        if config.name.endswith(".toml"):
            import toml

            clist = toml.load(config)["pidcontroller"]
        elif config.name.endswith(".json"):
            import json

            clist = json.load(config)["pidcontroller"]
        elif config.name.endswith(".yaml"):
            import yaml

            clist = yaml.safe_load(config)["pidcontroller"]
        else:
            print(f"ERROR: Unrecognised config file type: {config.name}")
            print("       Accepted types are: toml, json, yaml")
        for cobj in clist:
            configs.append(cobj)

    app = QApplication([])
    mainWindow = MainWindow(configs)
    mainWindow.show()
    return app.exec()
