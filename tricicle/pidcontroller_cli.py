"""PIDController CLI module. See ``pidcontroller --help`` for description, or read Click
decorators.

This module is not explicitly documented here, as it is expected the CLI self-
documentation should be sufficient.

See also the `PIDControllerl` class.
"""

import click
import curses
import time
from curses.textpad import Textbox
import logging
import threading
from typing import Tuple
from math import inf

from icicle.instrument import Instrument
from icicle.hmp4040 import HMP4040  # noqa: F401
from icicle.tti import TTI  # noqa: F401
from icicle.itkdcsinterlock import ITkDCSInterlock  # noqa: F401
from icicle.keysighte3633a import KeysightE3633A  # noqa: F401
from icicle.hp34401a import HP34401A  # noqa: F401
from icicle.keithley2000 import Keithley2000  # noqa: F401
from icicle.keithley2410 import Keithley2410  # noqa: F401
from icicle.relay_board import RelayBoard  # noqa: F401
from icicle.binder_climate_chamber import Binder  # noqa: F401
from icicle.mp1 import MP1  # noqa: F401
from icicle.lauda import Lauda  # noqa: F401
from icicle.cli_utils import with_instrument, print_output, verbosity

from tricicle.pidcontroller import PIDController

logger = logging.getLogger(__name__)

# ================== CLI METHODS ===================
# Below are script-like functions designed to be called to replace individual python
# scripts. They generally should:
# 1. instantiate and connect to instrument without resetting it
# 2. perform action
# 3. disconnect without ramping down or terminating instrument


@click.group()
@click.argument(
    "power_instrument",
    type=click.Choice(Instrument.registered_classes),
    metavar="POWER_INSTRUMENT_CLASS",
)
@click.argument("power_channel", type=int, metavar="POWER_CHANNEL")
@click.argument(
    "power_field", type=click.Choice(("voltage", "current")), metavar="voltage|current"
)
@click.argument(
    "measure_instrument",
    type=click.Choice(Instrument.registered_classes),
    metavar="MEASURE_INSTRUMENT_CLASS",
)
@click.argument("measure_channel", type=int, metavar="CHANNEL")
@click.argument("measure_type", type=str, metavar="MEASUREMENT_TYPE")
@click.option(
    "-v", "--verbose", count=True, help="Verbose output (-v = INFO, -vv = DEBUG)"
)
@click.option(
    "-P",
    "--power-resource",
    metavar="TARGET",
    type=str,
    default="ASRL2::INSTR",
    help="VISA resource address (default: ASRL2::INSTR)",
)
@click.option(
    "-M",
    "--measure-resource",
    metavar="TARGET",
    type=str,
    default="ASRL5::INSTR",
    help="VISA resource address (default: ASRL5::INSTR)",
)
@click.option(
    "-S",
    "--simulate",
    is_flag=True,
    help="Use pyvisa_sim backend as simulated instruments.",
)
@click.option(
    "-T",
    "--tunings",
    metavar="Kp Ki Kd",
    type=(float, float, float),
    default=(1.0, 0.1, 0.05),
    help="Kp, Ki, Kd tuning parameters for PID loop equation",
)
@click.option(
    "-X",
    "--setpoint",
    metavar="SETPOINT",
    type=float,
    default=0.0,
    help="Initial setpoint for PID loop (default: 0)",
)
@click.option(
    "-I",
    "--starting-output",
    metavar="VALUE",
    type=float,
    default=0.0,
    help="Starting output for channel/PID equation",
)
@click.option(
    "-D",
    "--sample-time",
    metavar="DELAY",
    type=float,
    default="0.01",
    help="Delay between update step of PID equation and power channel (in s)",
)
@click.option(
    "-L",
    "--limits",
    metavar="LOW HIGH",
    type=(float, float),
    default=(-inf, inf),
    help="Limits on output variable",
)
@click.option(
    "--proportional-on-measurement",
    is_flag=True,
    help=(
        "To eliminate overshoot in certain types of systems, calculate the "
        "proportional term directly on the measurement instead of the error."
    ),
)
@click.option(
    "--no-differential-on-measurement",
    is_flag=True,
    help=(
        "By default the differential term is calculated on the measurement rather "
        "than the error; this can be disabled using this flag."
    ),
)
@click.pass_context
def cli(
    ctx,
    power_instrument: str,
    power_channel: int,
    power_field: str,
    measure_instrument: str,
    measure_channel: int,
    measure_type: str,
    power_resource: str,
    measure_resource: str,
    verbose: int,
    simulate: bool,
    tunings: Tuple[float, float, float],
    setpoint: float,
    starting_output: float,
    sample_time: float,
    limits: Tuple[float, float],
    proportional_on_measurement: bool,
    no_differential_on_measurement: bool,
):
    logging.basicConfig(level=verbosity(verbose))

    # Instantiate power instrument
    power_cls = Instrument.registered_classes[power_instrument]
    powerchannel = power_cls(resource=power_resource, sim=simulate).channel(
        Instrument.PowerChannel, power_channel
    )

    # Instantiate measure instrument
    measure_cls = Instrument.registered_classes[measure_instrument]
    measurechannel = measure_cls(resource=measure_resource, sim=simulate).channel(
        Instrument.MeasureChannel, measure_channel, measure_type=measure_type
    )

    ctx.obj = PIDController(
        measure_channel=measurechannel,
        set_channel=powerchannel,
        set_field=power_field,
        Kp=tunings[0],
        Ki=tunings[1],
        Kd=tunings[2],
        setpoint=setpoint,
        sample_time=sample_time,
        output_limits=limits,
        proportional_on_measurement=proportional_on_measurement,
        differential_on_measurement=not no_differential_on_measurement,
        starting_output=starting_output,
    )


@cli.command(
    "identify",
    help=("Run identify command on devices controlled by this PID loop."),
)
@with_instrument
@print_output
def cli_identify(pid):
    return (
        f"{pid.measure_channel.instrument.identify()}\n"
        f"{pid.set_channel.instrument.identify()}"
    )


@cli.command(
    "loop",
    help=("Start (non-interactive) PIDController loop."),
)
@click.argument(
    "setpoint",
    metavar="SETPOINT",
    type=float,
)
@with_instrument
def cli_loop(pid, setpoint: float):
    SPACING = 17
    pid.setpoint = setpoint
    print(
        "\t".join(
            [
                s.ljust(SPACING)
                for s in ("Setpoint", "Measured Value", "Controlled Value")
            ]
        )
    )
    try:
        while True:
            meas, cont = pid()
            print(
                "\t".join([str(s).ljust(SPACING) for s in (setpoint, meas, cont)]),
                end="\r",
            )
    except KeyboardInterrupt:
        print("KeyboardInterrupt caught. Exiting...")


# ----------------------- the below is not good --------------------------


@cli.command(
    "interactive",
    help=("Start interactive PIDController loop."),
)
@click.pass_context
def cli_interactive(ctx):
    class PIDThread(threading.Thread):

        def __init__(
            self,
            pid_controller: PIDController,
            lock: threading.Lock,
            name: str = "pid-controller-thread",
        ):
            self.pid = pid
            self.lock = lock
            rows, cols = stdscr.getmaxyx()
            self.window = curses.newpad(4, cols)
            self.stop = False
            super().__init__(name=name)

        def run(self):
            SPACING = 17
            with self.lock:
                self.window.addstr(
                    1,
                    2,
                    "\t".join(
                        [
                            s.ljust(SPACING)
                            for s in (
                                "Time",
                                "Setpoint",
                                "Measured Value",
                                "Controlled Value",
                            )
                        ]
                    ),
                )
            try:
                while not self.stop:
                    meas, cont = pid()
                    self.window.addstr(
                        2,
                        2,
                        "\t".join(
                            (
                                time.strftime("%x %X").ljust(SPACING),
                                *[
                                    str(s).ljust(SPACING)
                                    for s in (self.pid.setpoint, meas, cont)
                                ],
                            )
                        ),
                    )
                    if not self.lock.locked():
                        with self.lock:
                            rows, cols = stdscr.getmaxyx()
                            self.window.refresh(0, 0, 0, 0, 3, cols)

            except KeyboardInterrupt:
                with self.lock:
                    self.window.addstr(
                        3,
                        2,
                        "KeyboardInterrupt caught in pid-controller-thread. Exiting...",
                    )
                self.stop = True

    def enter_is_terminate(x):
        if x == 10:
            x = 7
        return x

    stdscr = curses.initscr()
    try:
        with ctx.obj as pid:
            lock = threading.Lock()
            pidthread = PIDThread(pid, lock, stdscr)
            pidthread.start()

            try:
                while not pidthread.stop:
                    continue
                    rows, cols = stdscr.getmaxyx()
                    with lock:
                        stdscr.addstr(rows - 1, 0, " > ")
                        window = curses.newwin(0, cols - 3, rows - 2, 3)
                        textbox = Textbox(window)
                    command = textbox.edit(enter_is_terminate)
                    exit(1)

                    if command in ("q", "exit"):
                        pidthread.stop = True
                        pidthread.join()

            except KeyboardInterrupt:
                pidthread.stop = True
                pidthread.join()
    finally:
        curses.endwin()


@click.group()
@click.pass_context
def interactive(ctx):
    pass


@interactive.command(
    "setpoint", help="Set setpoint to SETPOINT, or read if not specified"
)
@click.argument(
    "setpoint", metavar="SETPOINT", type=float, default=None, required=False
)
@print_output
@click.pass_obj
def interactive_setpoint(pidthread, setpoint: float):
    if setpoint is not None:
        pidthread.pid.setpoint = setpoint
    return pidthread.pid.setpoint
