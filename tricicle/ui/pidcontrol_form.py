from threading import Lock
from time import sleep

from icicle.instrument import Instrument

from tricicle.pidcontroller import PIDController
from tricicle.ui.pidconfig_form import PIDConfigForm

from tricicle.qtloader import load_qt, load_ui

QtCore = load_qt("QtCore")
QtWidgets = load_qt("QtWidgets")
Ui_pidcontrol_form = load_ui("pidcontrol_form", "Ui_pidcontrol_form")


class PIDControlForm(QtWidgets.QWidget, Ui_pidcontrol_form):

    def __init__(self, index, config=None, parent=None):
        super().__init__(parent=parent)
        self.index = index
        self.setupUi(self)

        powerchannel = None
        measurechannel = None

        if config:
            power_instrument = config.pop("power_instrument")
            power_channel = config.pop("power_channel")
            power_resource = config.pop("power_resource")
            measure_instrument = config.pop("measure_instrument")
            measure_channel = config.pop("measure_channel")
            measure_type = config.pop("measure_type")
            measure_resource = config.pop("measure_resource")
            simulate = config.pop("simulate")
            config["set_field"] = config.pop("power_type")

            # Instantiate power instrument
            power_cls = Instrument.registered_classes[power_instrument]
            powerchannel = power_cls(resource=power_resource, sim=simulate).channel(
                Instrument.PowerChannel, power_channel
            )

            # Instantiate measure instrument
            measure_cls = Instrument.registered_classes[measure_instrument]
            measurechannel = measure_cls(
                resource=measure_resource, sim=simulate
            ).channel(
                Instrument.MeasureChannel, measure_channel, measure_type=measure_type
            )
        else:
            config = {}

        self.pidcontroller = PIDController(
            measure_channel=measurechannel,
            set_channel=powerchannel,
            auto_mode=False,
            **config,
        )
        self.lock = Lock()
        self.index_label.setText(f"PID\nController\n{index}")
        self.start_pushButton.clicked.connect(self.start_pidcontroller)
        self.setpoint_doubleSpinBox.valueChanged.connect(self.setpoint_changed)
        self.config_form = PIDConfigForm(
            self.index, self.pidcontroller, self.lock, False, parent=None
        )
        self.config_pushButton.clicked.connect(self.config_form.show)

    def start_pidcontroller(self):
        if (
            self.pidcontroller.measure_channel is None
            or self.pidcontroller.set_channel is None
        ):
            alert = QtWidgets.QMessageBox(
                QtWidgets.QMessageBox.Icon.Warning,
                "Warning: Invalid channel",
                "Warning: Invalid or unspecified measure or power channel for "
                "this PIDController! Cannot start.",
                parent=self,
            )
            alert.exec()
            return
        self.pidcontroller.set_auto_mode(True)
        with self.pidcontroller:
            self.pidcontroller.on()
        self.pidcontrol_thread = QtCore.QThread()
        self.pidcontrol_worker = PIDControllerThread(
            self.pidcontrol_thread, self.pidcontroller, self.lock
        )
        self.pidcontrol_worker.moveToThread(self.pidcontrol_thread)
        self.pidcontrol_thread.started.connect(self.pidcontrol_worker.run)
        self.pidcontrol_worker.value.connect(self.pidcontroller_value)
        self.pidcontrol_worker.control.connect(self.pidcontroller_control)
        self.pidcontrol_worker.finished.connect(self.pidcontroller_stopped)
        self.pidcontrol_thread.finished.connect(self.pidcontroller_thread_stopped)
        # Update button state and slot
        _translate = QtCore.QCoreApplication.translate
        self.start_pushButton.setText(_translate("pidcontrol_form", "Stop"))
        self.start_pushButton.clicked.disconnect()
        self.start_pushButton.clicked.connect(self.stop_pidcontroller)
        self.config_form.disable_when_running()
        # And go...
        self.pidcontrol_thread.start()

    def stop_pidcontroller(self):
        if self.pidcontrol_thread.isRunning():
            with self.lock:
                self.pidcontrol_worker.running = False
            _translate = QtCore.QCoreApplication.translate
            self.start_pushButton.setText(_translate("pidcontrol_form", "Kill"))
        else:
            self.pidcontrol_thread.quit()
            self.pidcontroller_stopped()

    def pidcontroller_stopped(self):
        self.pidcontrol_thread.wait()
        self.pidcontroller.set_auto_mode(False)
        with self.pidcontroller:
            self.pidcontroller.off()
        del self.pidcontrol_thread
        del self.pidcontrol_worker

    def pidcontroller_thread_stopped(self):
        _translate = QtCore.QCoreApplication.translate
        self.start_pushButton.setText(_translate("pidcontrol_form", "Start"))
        self.start_pushButton.clicked.disconnect()
        self.start_pushButton.clicked.connect(self.start_pidcontroller)
        self.config_form.enable_when_not_running()

    def pidcontroller_value(self, value):
        self.value_lcdNumber.display(value)

    def pidcontroller_control(self, control):
        self.value_lcdNumber_2.display(control)

    def setpoint_changed(self, setpoint):
        with self.lock:
            self.pidcontroller.setpoint = setpoint


class PIDControllerThread(QtCore.QObject):
    # Signals
    finished = QtCore.pyqtSignal()
    value = QtCore.pyqtSignal(float)
    control = QtCore.pyqtSignal(float)

    def __init__(self, thread, pidcontroller, lock):
        super().__init__()
        self.running = False
        self.pidcontroller = pidcontroller
        self.lock = lock
        self.thread = thread

    def run(self):
        self.running = True
        with self.pidcontroller as pid:
            while self.running:
                with self.lock:
                    value, control = pid()
                    self.value.emit(value)
                    self.control.emit(control)
                sleep(self.pidcontroller.sample_time / 10.0)
        self.finished.emit()
        self.thread.exit()
