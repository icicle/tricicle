import sys
from importlib.metadata import version

from .pidcontrol_form import PIDControlForm

from tricicle.qtloader import load_qt, load_ui

QtCore = load_qt("QtCore")
QtWidgets = load_qt("QtWidgets")
Ui_mainWindow = load_ui("mainwindow", "Ui_mainWindow")


class MainWindow(QtWidgets.QMainWindow, Ui_mainWindow):

    def __init__(self, pidcontrol_configs):
        super().__init__()

        self.setupUi(self)

        self.pidcontrols = list()

        for pidcontrol_config in pidcontrol_configs:
            self.add_pid_controller(pidcontrol_config)

        self.addPIDController_pushButton.clicked.connect(self.add_pid_controller)

        self.pushButton.clicked.connect(self.exit)

    def retranslateUi(self, mainWindow):
        _translate = QtCore.QCoreApplication.translate
        mainWindow.setWindowTitle(
            _translate("mainWindow", f'TRICICLE {version("tricicle")} - PID Controller')
        )
        self.version_label.setText(
            _translate(
                "mainWindow",
                f'PIDController  |  TRICICLE {version("tricicle")}  |  '
                f'ICICLE {version("icicle")}',
            )
        )
        self.addPIDController_pushButton.setText(
            _translate("mainWindow", "Add PIDController")
        )
        self.pushButton.setText(_translate("mainWindow", "Exit"))

    def exit(self):
        sys.exit()

    def add_pid_controller(self, pidcontrol_config=None):
        pidcontrol = PIDControlForm(
            len(self.pidcontrols), pidcontrol_config, parent=self.centralwidget
        )
        self.pidcontrols.append(pidcontrol)
        self.verticalLayout.insertWidget(len(self.pidcontrols), pidcontrol)
