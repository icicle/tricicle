import math

from icicle.instrument import Instrument

from tricicle.qtloader import load_qt, load_ui

QtCore = load_qt("QtCore")
QtWidgets = load_qt("QtWidgets")
Ui_pidconfig_form = load_ui("pidconfig_form", "Ui_pidconfig_form")


class PIDConfigForm(QtWidgets.QWidget, Ui_pidconfig_form):

    def __init__(self, index, pidcontroller, lock, pidcontroller_running, parent=None):
        super().__init__(parent=parent)
        self.index = index
        self.setupUi(self)
        self.pidcontroller = pidcontroller
        self.lock = lock
        self.pidcontroller_running = pidcontroller_running
        self.populate()
        self.measureChannelInstrument_comboBox.currentTextChanged.connect(
            self.update_measure_types
        )
        self.buttonBox.clicked.connect(self.button_clicked)

    def button_clicked(self, button):
        buttonrole = self.buttonBox.buttonRole(button)
        if buttonrole == QtWidgets.QDialogButtonBox.ButtonRole.AcceptRole:
            self.ok()
        if buttonrole == QtWidgets.QDialogButtonBox.ButtonRole.ApplyRole:
            self.apply()
        if buttonrole == QtWidgets.QDialogButtonBox.ButtonRole.RejectRole:
            self.cancel()

    def update_measure_types(self, value):
        if not value:
            return
        self.measureChannelType_comboBox.clear()
        cls = Instrument.registered_classes[value]
        self.measureChannelType_comboBox.addItems(
            [k for k in cls.MEASURE_TYPES.keys() if hasattr(cls, "MEASURE_TYPES")]
        )

    def disable_when_running(self):
        for obj in (
            self.powerChannelInstrument_comboBox,
            self.powerChannelResource_lineEdit,
            self.powerChannelChannel_spinBox,
            self.powerChannelType_comboBox,
            self.measureChannelInstrument_comboBox,
            self.measureChannelResource_lineEdit,
            self.measureChannelChannel_spinBox,
            self.measureChannelType_comboBox,
            self.simulateDevices_checkBox,
        ):
            obj.setEnabled(False)
        self.pidcontroller_running = True

    def enable_when_not_running(self):
        for obj in (
            self.powerChannelInstrument_comboBox,
            self.powerChannelResource_lineEdit,
            self.powerChannelChannel_spinBox,
            self.powerChannelType_comboBox,
            self.measureChannelInstrument_comboBox,
            self.measureChannelResource_lineEdit,
            self.measureChannelChannel_spinBox,
            self.measureChannelType_comboBox,
            self.simulateDevices_checkBox,
        ):
            obj.setEnabled(True)
        self.pidcontroller_running = False

    def populate(self):
        powerchannel = self.pidcontroller.set_channel

        self.powerChannelInstrument_comboBox.clear()
        powerchannels = [
            k
            for k, cls in Instrument.registered_classes.items()
            if cls.PowerChannel is not Instrument.PowerChannel
        ]
        self.powerChannelInstrument_comboBox.addItems(powerchannels)

        if powerchannel:
            powerchannel_instrument = type(powerchannel.instrument).__name__
            self.powerChannelInstrument_comboBox.setCurrentText(powerchannel_instrument)
            self.powerChannelChannel_spinBox.setValue(powerchannel.channel)
            self.powerChannelResource_lineEdit.setText(powerchannel.instrument.resource)
        else:
            self.powerChannelChannel_spinBox.setValue(1)
            self.powerChannelResource_lineEdit.setText("<Resource>")

        self.powerChannelType_comboBox.clear()
        self.powerChannelType_comboBox.addItems(("voltage", "current"))
        self.powerChannelType_comboBox.setCurrentText(self.pidcontroller.set_field)

        measurechannel = self.pidcontroller.measure_channel

        self.measureChannelInstrument_comboBox.clear()
        measurechannels = [
            k
            for k, cls in Instrument.registered_classes.items()
            if cls.MeasureChannel is not Instrument.MeasureChannel
        ]
        self.measureChannelInstrument_comboBox.addItems(measurechannels)

        if measurechannel:
            measurechannel_instrument = type(measurechannel.instrument).__name__
            self.measureChannelInstrument_comboBox.setCurrentText(
                measurechannel_instrument
            )
            self.update_measure_types(measurechannel_instrument)
            self.measureChannelChannel_spinBox.setValue(measurechannel.channel)
            self.measureChannelType_comboBox.setCurrentText(measurechannel.measure_type)
            self.measureChannelResource_lineEdit.setText(
                measurechannel.instrument.resource
            )
        else:
            self.update_measure_types(
                self.measureChannelInstrument_comboBox.currentText()
            )
            self.measureChannelChannel_spinBox.setValue(1)
            self.measureChannelResource_lineEdit.setText("<Resource>")

        if powerchannel or measurechannel:
            ch = powerchannel or measurechannel
            self.simulateDevices_checkBox.setChecked(ch.instrument.sim)
        else:
            self.simulateDevices_checkBox.setChecked(False)

        self.pollRate_doubleSpinBox.setValue(1.0 / self.pidcontroller.sample_time)
        self.initialOutput_doubleSpinBox.setValue(self.pidcontroller.components[1])
        if self.pidcontroller.output_limits:
            self.minOutput_doubleSpinBox.setValue(self.pidcontroller.output_limits[0])
            self.maxOutput_doubleSpinBox.setValue(self.pidcontroller.output_limits[1])
        else:
            self.minOutput_doubleSpinBox.setValue(-math.inf)
            self.maxOutput_doubleSpinBox.setValue(math.inf)
        self.kp_doubleSpinBox.setValue(self.pidcontroller.Kp)
        self.ki_doubleSpinBox.setValue(self.pidcontroller.Ki)
        self.kd_doubleSpinBox.setValue(self.pidcontroller.Kd)
        self.propOnMeasurement_checkBox.setChecked(
            self.pidcontroller.proportional_on_measurement
        )
        self.diffOnError_checkBox.setChecked(
            not self.pidcontroller.differential_on_measurement
        )

    def apply(self):
        with self.lock:
            if not self.pidcontroller_running:
                power_instrument = self.powerChannelInstrument_comboBox.currentText()
                power_channel = self.powerChannelChannel_spinBox.value()
                power_resource = self.powerChannelResource_lineEdit.text()
                measure_instrument = (
                    self.measureChannelInstrument_comboBox.currentText()
                )
                measure_channel = self.measureChannelChannel_spinBox.value()
                measure_type = self.measureChannelType_comboBox.currentText()
                measure_resource = self.measureChannelResource_lineEdit.text()
                simulate = self.simulateDevices_checkBox.isChecked()

                # Instantiate power instrument
                power_cls = Instrument.registered_classes[power_instrument]
                self.pidcontroller.set_channel = power_cls(
                    resource=power_resource, sim=simulate
                ).channel(Instrument.PowerChannel, power_channel)
                self.pidcontroller.set_field = (
                    self.powerChannelType_comboBox.currentText()
                )

                # Instantiate measure instrument
                measure_cls = Instrument.registered_classes[measure_instrument]
                self.pidcontroller.measure_channel = measure_cls(
                    resource=measure_resource, sim=simulate
                ).channel(
                    Instrument.MeasureChannel,
                    measure_channel,
                    measure_type=measure_type,
                )

            self.pidcontroller.sample_time = 1.0 / self.pollRate_doubleSpinBox.value()
            self.pidcontroller.new_starting_control_value(
                self.initialOutput_doubleSpinBox.value()
            )
            self.pidcontroller.output_limits = (
                self.minOutput_doubleSpinBox.value(),
                self.maxOutput_doubleSpinBox.value(),
            )
            self.pidcontroller.Kp = self.kp_doubleSpinBox.value()
            self.pidcontroller.Ki = self.ki_doubleSpinBox.value()
            self.pidcontroller.Kd = self.kd_doubleSpinBox.value()
            self.pidcontroller.proportional_on_measurement = (
                self.propOnMeasurement_checkBox.isChecked()
            )
            self.pidcontroller.differential_on_measurement = (
                not self.diffOnError_checkBox.isChecked()
            )

    def cancel(self):
        # reset to current state
        self.populate()
        self.close()

    def ok(self):
        self.apply()
        self.close()

    def retranslateUi(self, pidconfig_form):
        _translate = QtCore.QCoreApplication.translate
        pidconfig_form.setWindowTitle(
            _translate("pidconfig_form", f"Configure PIDController {self.index}")
        )
        self.pollRate_label.setText(_translate("pidconfig_form", "Poll rate (Hz)"))
        self.initialOutput_label.setText(_translate("pidconfig_form", "Initial output"))
        self.minOutput_label.setText(_translate("pidconfig_form", "Min output"))
        self.maxOutput_label.setText(_translate("pidconfig_form", "Max output"))
        self.powerChannelResource_lineEdit.setText(
            _translate("pidconfig_form", "<Resource>")
        )
        self.measureChannelResource_lineEdit.setText(
            _translate("pidconfig_form", "<Resource>")
        )
        self.tunings_label.setText(_translate("pidconfig_form", "Tunings"))
        self.propOnMeasurement_checkBox.setText(
            _translate("pidconfig_form", "Calculate prop. term on measurement")
        )
        self.diffOnError_checkBox.setText(
            _translate("pidconfig_form", "Calculate diff. term on error")
        )
        self.kp_label.setText(_translate("pidconfig_form", "Kp"))
        self.ki_label.setText(_translate("pidconfig_form", "Ki"))
        self.kd_label.setText(_translate("pidconfig_form", "Kd"))
        self.measureChannel_label.setText(
            _translate("pidconfig_form", "MeasureChannel")
        )
        self.powerChannel_label.setText(_translate("pidconfig_form", "PowerChannel"))
        self.simulateDevices_checkBox.setText(
            _translate("pidconfig_form", "Simulate Devices")
        )
