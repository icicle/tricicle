TRICICLE - Thermal Regulation ICICLE Extension
==========================================================================

[![Version: 0.0.1](https://img.shields.io/badge/version-0.0.1-green)](https://gitlab.cern.ch/icicle/tricicle/-/tree/master)
[![ICICLE: 1.3.0-RC](https://img.shields.io/badge/ICICLE-1.3.0%20RC-orange)](https://gitlab.cern.ch/icicle/icicle/-/tree/1.3.0-RC)
[![Pipeline: master](https://gitlab.cern.ch/icicle/tricicle/badges/master/pipeline.svg)](https://gitlab.cern.ch/icicle/icicle/-/tree/0.0.1)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
<!--[![Docs: sphinx](https://img.shields.io/badge/docs-sphinx-blue)](https://gitlab.cern.ch/icicle/icicle/-/jobs/artifacts/1.3.0-RC/browse?job=docs)-->

This library extends [ICICLE](https://gitlab.cern.ch/icicle/icicle) with
some additional tools for thermal regulation and temperature management.

<!--Read the full documentation at https://icicle.docs.cern.ch/.-->

This package provides the following extensions:
- PID Controller
    - accepts `PowerChannel` and `MeasureChannel` objects, and sets
      control value on the `PowerChannel` to achieve a given setpoint
      on the MeasureChannel
    - can be run as a loop using the `pidcontroller` CLI hook, or with
      a Qt6 GUI exposed by the `pidcontroller_ui` executable

## Extension Last-Known-Tested Versions

Documented here is the last version that instruments have been explicitly tested
on (since many developers don't have access to all types of instruments and
cannot test possibly breaking changes to all). Patch-level changes should not
break existing instruments or functionality.

| Instrument             | LKT Version | Tested By        | Date       |
| ---------------------- | ----------- | ---------------- | ---------- |
| `PIDController`        | ongoing     | S. Koch          | ongoing    |
|                        |             |                  |            |
| `ICICLE compatibility` | 1.3.0-RC    | S. Koch          | 2024-03-27 |


## Installation

Python >= 3.7 is required. Python 3.9 or higher is recommended.

ICICLE is recommended to be installed from the
[git repo](https://gitlab.cern.ch/icicle/icicle), and version `1.3.0-RC` is
required at minimum to expose the `Channel` high-level interfaces. See the
associated documentation for instructions.

It is recommended to install into a virtual environment - for example:
```
python3 -m venv python3-env
. python3-env/bin/activate
```

After installing ICICLE, installation is performed using the `setup.py` script:
```
python3 -m pip install "."
```
If you would like to be able to modify the source files, add a `-e` flag.

To install the code formatting libraries required for developing and
contributing, please install with the `[dev]` optional dependency.

<!--If you need
to build documentation, you can install the correct `sphinx` version and
dependencies via the `[docs]` optional dependency.-->


## Command Line Interface - Usage

For each extension, simply type `<extension> --help` for a full list of commands
and arguments. For example:
```
$ pidcontroller --help

Usage: pidcontroller [OPTIONS] POWER_INSTRUMENT_CLASS POWER_CHANNEL
                     voltage|current MEASURE_INSTRUMENT_CLASS CHANNEL
                     MEASUREMENT_TYPE COMMAND [ARGS]...

Options:
  -v, --verbose                   Verbose output (-v = INFO, -vv = DEBUG)
  -P, --power-resource TARGET     VISA resource address (default:
                                  ASRL2::INSTR)
  -M, --measure-resource TARGET   VISA resource address (default:
                                  ASRL5::INSTR)
  -S, --simulate                  Use pyvisa_sim backend as simulated
                                  instruments.
  -T, --tunings Kp Ki Kd          Kp, Ki, Kd tuning parameters for PID loop
                                  equation
  -X, --setpoint SETPOINT         Initial setpoint for PID loop (default: 0)
  -I, --starting-output VALUE     Starting output for channel/PID equation
  -D, --sample-time DELAY         Delay between update step of PID equation
                                  and power channel (in s)
  -L, --limits LOW HIGH           Limits on output variable
  --proportional-on-measurement   To eliminate overshoot in certain types of
                                  systems, calculate the proportional term
                                  directly on the measurement instead of the
                                  error.
  --no-differential-on-measurement
                                  By default the differential term is
                                  calculated on the measurement rather than
                                  the error; this can be disabled using this
                                  flag.
  --help                          Show this message and exit.

Commands:
  identify     Run identify command on devices controlled by this PID loop.
  interactive  Start interactive PIDController loop.
  loop         Start (non-interactive) PIDController loop.
```

Individual commands can also be combined with `--help` for further information.


## Graphical Interfaces

The library offers some graphical interface extensions for ICICLE - these
rely on Qt5/6, which may require installation via a local package manager.

## Simulating devices

The extensions can generally utilise the ICICLE simulation backend for
supported devices. To run the simulation, add the `-S` flag to the start
of relevant commands, and replace the resource address with an appropriate
ASRL number from the `icicle/sim/visa_instrument.yaml` simulation file.

More details may be found in the
[ICICLE Documentation](https://icicle.docs.cern.ch).

## Developing/Contributing

Plese make sure to install the package in editable mode with the `[dev]`
optional dependency, and enable the pre-commit hooks in the git repository
after installation via:
```
pre-commit install
```

This will run the [Black](https://black.readthedocs.io/en/stable/) automatic
code format tool, as well as `pydocformatter` and a few other simple
reformatting tools on commit.

Please use a local PEP8-compliant linter while coding - we recommend
[Flake8](https://flake8.pycqa.org/en/latest/), which can be integrated into
most IDEs, and has a project-specific ruleset defined in
`pyproject.toml`.

A more formal set of MR requirements and approval/review hierarchy will be added
soon.
